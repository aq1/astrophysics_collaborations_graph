# Astrophysics collaboration network graph analysis

* [data](https://snap.stanford.edu/data/ca-AstroPh.txt.gz)
* [info](https://snap.stanford.edu/data/ca-AstroPh.html)

[arXiv](https://arxiv.org/) allows researchers to publish freely articles in their domain. The data lists the collaborations between authors in Astrophysics for 10 years.

Here I am using Spark with Scala to analyze this graph.

key point: the astrophysics graph is a small world network:
* Low density: 0.00112
* High clustering coefficient: 0.63
* The diameter is of only: 14
* Degrees distributed on a large range:
* 1 connected subgraph accounts for more than 95% of the nodes.
* The nodes are grouped into clusters.

![Degrees distribution](https://gitlab.com/aq1/astrophysics_collaborations_graph/raw/master/degrees_distribution.png?inline=false)

![viz](https://gitlab.com/aq1/astrophysics_collaborations_graph/raw/master/viz.png?inline=false)
