// Astro Physics collaboration network graph analysis


// Download data

// wget https://snap.stanford.edu/data/ca-AstroPh.txt.gz
// gunzip -k ca-AstroPh.txt.gz 

// head ca-AstroPh.txt
// # Directed graph (each unordered pair of nodes is saved once): CA-AstroPh.txt 
// # Collaboration network of Arxiv Astro Physics category (there is an edge if authors coauthored at least one paper)
// # Nodes: 18772 Edges: 396160
// # FromNodeId	ToNodeId
// 84424	276
// 84424	1662
// 84424	5089
// 84424	6058
// 84424	6229
// 84424	10639

// cat ca-AstroPh.txt | wc -l
// 396164

// If you remove the 4 lines of headers, you get the number of directed edges of coauthorship (+ 4 lines of headers). According to the description it represents an undirected graph. So, the information is redundant.
// Divided by 2, we get some edges (198,080). This is slightly lower than the number provided 198110.

// This is quite disappointing to see no way of linking this graph to paper verbose tags. We are expecting to see communities about a subject, e.g., people writing about black holes. Here, with the data provided, we can't put names on those communities.


// CSV normalization

// cat ca-AstroPh.txt | sed "/^#/d" | gsed "s/\t/,/" > ca-AstroPh.csv
// This could also be done by configuring the load statement in scala later


// Read data

import org.apache.spark.sql.types._
val rawDataSchema = StructType(Array(
  StructField("from", IntegerType, true),
  StructField("to", IntegerType, true)
))

val rawData = spark.read.
  format("csv").
  schema(rawDataSchema).
  load("ca-AstroPh.csv")
rawData.show(2)
// +-----+----+
// | from|  to|
// +-----+----+
// |84424| 276|
// |84424|1662|
// +-----+----+
// only showing top 2 rows

rawData.getClass
// org.apache.spark.sql.Dataset

val vertices = rawData.
  flatMap(row => Array(row.getInt(0), row.getInt(1))).
  distinct.
  map(id => (id.toLong, id.toLong))

vertices.count
// res1: Long = 18772

vertices.show(2)
// +-----+-----+                                                                   
// |   _1|   _2|
// +-----+-----+
// |19204|19204|
// |44022|44022|
// +-----+-----+

import org.apache.spark.graphx._

val edges = rawData.map(row => {
  val ids = Array(row.getInt(0), row.getInt(1))
  Edge(ids(0), ids(1), 1.0)
})

edges.show(2)
// +-----+-----+----+                                                              
// |srcId|dstId|attr|
// +-----+-----+----+
// |63552|84424|   1|
// |84424|88768|   1|
// +-----+-----+----+

vertices.getClass

val astroGraph = Graph(vertices.rdd, edges.rdd)

astroGraph.cache()

astroGraph.vertices.count
// res15: Long = 18772


// Analyze graph connectivity

val connectedComponentGraph: Graph[VertexId, Int] = astroGraph.connectedComponents()

def sortedConnectedComponents(connectedComponents: Graph[VertexId, _]): Seq[(VertexId, Long)] = {
  val componentCounts = connectedComponents.vertices.map(_._2).countByValue
  componentCounts.toSeq.sortBy(_._2).reverse
}

val componentCounts = sortedConnectedComponents(connectedComponentGraph)

componentCounts.size
// res17: Int = 290 There are 290 connected components
//
componentCounts.take(10).foreach(println)
// (3,17903) id:3 17903 vertices in this subgraph, this is the subgraph we are going to study. It represents 95.4% of the nodes.
// (17002,18)
// (20816,12)
// (4314,10)
// (19385,10)
// (33128,9)
// (358,8)
// (25350,8)
// (7439,8)
// (20505,7)

// => There 1 big subgraph which connects most of the vertices


// Analyze topic degrees

val degrees: VertexRDD[Int] = astroGraph.degrees.cache()

degrees.map(_._2).stats()
// res20: org.apache.spark.util.StatCounter = (count: 18772, mean: 21.106968, stdev: 30.571342, max: 504.000000, min: 1.000000)
// => (count: 13721, mean: 31.155892, stdev: 65.497591, max: 2596.000000, min: 1.000000)

// Some vertices are connected to a lot of others vertices, e.g., the node with a degree of 2596.

degrees.take(10)
// res28: Array[(org.apache.spark.graphx.VertexId, Int)] = Array((25400,10), (49200,2), (66200,30), (29800,3), (86000,49), (87600,3), (123800,1), (27400,35), (69400,24), (58200,4))

// Lists the most connected nodes.

val ord = Ordering.by[(Long, Int), Int](_._2)
degrees.top(10)(ord).foreach(println)
// (53213,504) Some authors collaborate with a lot of peoples.
// (35290,427)
// (38109,420)
// (62821,418)
// (93504,387)
// (92790,385)
// (21718,369)
// (1086,362)
// (89732,351)
// (111161,350)

// Lists the less connected nodes.
degrees.top(2)(ord.reverse).foreach(println)
// (46600,1)
// (104000,1)
// The data is about coauthorship, so the minimum degree is 1.


// Average of shortest path lengths

// wget https://raw.githubusercontent.com/swordsmanliu/SparkStreamingHbase/master/lib/spark-core_2.11-1.5.2.logging.jar
// spark-shell --packages it.unipd.dei:graphx-diameter_2.11:0.1.0 --jars spark-core_2.11-1.5.2.logging.jar --total-executor-cores 8 --executor-memory = 500M

import it.unipd.dei.graphx.diameter.DiameterApproximation
val astroGraph = Graph(vertices.rdd, edges.rdd)
DiameterApproximation.run(astroGraph)
// => Too slow
// java.lang.OutOfMemoryError: Java heap space


// Global clustering coefficient

val triangleCountGraph = astroGraph.triangleCount() // connectivity to neighbours

triangleCountGraph.vertices.map(x => x._2).stats()
// res2: org.apache.spark.util.StatCounter = (count: 18772, mean: 215.977147, stdev: 532.311503, max: 11269.000000, min: 0.000000)

val maxTrisGraph = astroGraph.degrees.mapValues(d => d * (d - 1) / 2.0)

val clusterCoefGraph = triangleCountGraph.vertices.innerJoin(maxTrisGraph) {
  (vertexId, triCount, maxTris) => {
    if (maxTris == 0) 0
    else triCount / maxTris
  }
}

clusterCoefGraph.map(_._2).sum() / astroGraph.vertices.count()
// Double = 0.6302030664368092 

// This represents a high level of clusterization.


// Plot degrees

// spark-shell --packages org.vegas-viz:vegas_2.11:0.3.11,org.vegas-viz:vegas-spark_2.11:0.3.11

degrees.take(20)
// Array((25400,10), (49200,2)

degrees.count
// 18772

degrees.getClass
// spark.rdd

val degreesCounts = degrees.
  map(degree => (degree._2, 1)).
  reduceByKey((a, b) => a + b)

degreesCounts.take(10)
// Array[(Int, Int)] = Array((200,1), (1,1281), (202,1), (2,1719), (3,1595), (4,1266), (5,987), (6,811), (7,705), (208,1))

degreesCounts.getClass
// class org.apache.spark.rdd.ShuffledRDD

case class DegreeCount(degree: Int, count: Int)
val degreesCountsDF = degreesCounts.map {
  case (s0, s1) => DegreeCount(s0, s1)
}.toDF()

import vegas._
import vegas.render.WindowRenderer._
import vegas.sparkExt._

Vegas("Degrees", width=800, height=800).
  withDataFrame(degreesCountsDF).
  mark(Point).
  encodeX("degree", Quantitative).
  encodeY("count", Quantitative).
  show

import math._
import org.apache.spark.sql.functions.expr

val logDegreesCountsDF = degreesCountsDF.
  withColumn("log(degree)", expr("log10(degree)")).
  withColumn("log(count)", expr("log10(count)"))

Vegas("Degrees", width=800, height=800).
  withDataFrame(logDegreesCountsDF).
  mark(Point).
  encodeX("log(degree)", Quantitative).
  encodeY("log(count)", Quantitative).
  show

Vegas("Degrees", width=800, height=800).
  withDataFrame(degreesCountsDF).
  mark(Point).
  encodeX("degree", Quantitative, scale=Scale(Some(vegas.ScaleType.Log))).
  encodeY("count", Quantitative, scale=Scale(Some(vegas.ScaleType.Log))).
  show


// Graph density
// 2|E| / |V|(|V|-1)

def graphDensity(graph: org.apache.spark.graphx.Graph[Long,_]): Double = {
  val verticesCard = graph.vertices.count.toDouble
  println(verticesCard)
  val edgesCard = graph.edges.count.toDouble
  println(edgesCard)
  2 * edgesCard / (verticesCard * (verticesCard - 1))
}

graphDensity(astroGraph)

// res23: Double = 0.0011244455715955115
// This is not a dense graph.


// sparkling-graph

// => failure for community detections
// spark-shell \
//   --packages ml.sparkling:sparkling-graph_2.11:0.0.7 \
//   --jars spark-core_2.11-1.5.2.logging.jar \
//   --total-executor-cores 8

import sparkling.graph.operators.OperatorsDSL._
import ml.sparkling.graph.operators.OperatorsDSL._
import ml.sparkling.graph.operators.algorithms.pscan.PSCAN.ComponentID
import org.apache.spark.SparkContext
import org.apache.spark.graphx.Graph


// Louvain lib

// => failure for community detections
// https://github.com/Sotera/spark-distributed-louvain-modularity 
// gradle clean dist not working....


// Girvan Newman is not optimal
// https://gist.github.com/srirambaskaran/573927ee01f3673d3a9182bacbc9ed39

// https://github.com/athinggoingon/louvain-modularity 2015.... no readme


// Last try https://towardsdatascience.com/large-scale-graph-mining-with-spark-part-2-2c3d9ed15bb5


// GraphFrame LPA for community detections

// spark-shell \
// --packages graphframes:graphframes:0.6.0-spark2.3-s_2.11 \
// --total-executor-cores 8

import org.graphframes._
import org.graphframes.GraphFrame
import org.apache.spark.sql.types._

val rawDataSchema = StructType(Array(
  StructField("from", IntegerType, true),
  StructField("to", IntegerType, true)
))

val rawData = spark.read.
  format("csv").
  schema(rawDataSchema).
  load("ca-AstroPh.csv")

val vertices = rawData.
  flatMap(row => Array(row.getInt(0), row.getInt(1))).
  distinct.
  map(id => (id.toLong)).
  toDF("id")

val edges = rawData.map(row => {
  (row.getInt(0), row.getInt(1))
}).toDF("src", "dst")

val astroGraph = GraphFrame(vertices, edges)

val astroCommunities = astroGraph.labelPropagation.maxIter(50).run()

astroCommunities.map(row => row.getLong(1)).distinct.count
// 5 res22: Long = 1836 
// 10 res22: Long = 1519
// 20 res22: Long = 1196
// 50 res22: Long = 1139
// 100 res22: Long = 1139

astroCommunities.
  map(row => (row.getLong(1), 1L)).
  groupByKey(_._1).
  reduceGroups((a, b) => (a._1, a._2 + b._2)).
  map(_._2).
  collect().
  toSeq.
  sortBy(_._2).
  reverse.
  take(10).foreach(println)
// (19204,1763)                                                                    
// (114352,1537)
// (36739,582)
// (83171,342)
// (113481,241)
// (124862,225)
// (35020,215)
// (66502,215)
// (105274,209)
// (130322,192)

// communities vertices: community id + weight (number of vertices)

val astroCommunitiesVertices = astroCommunities.
  map(row => (row.getLong(1), 1L)).
  groupByKey(_._1).
  reduceGroups((a, b) => (a._1, a._2 + b._2)).
  map(_._2)

astroCommunitiesVertices.
  repartition(1).
  write.
  csv("vertices")

// communities edges: community id 1, community id 2, weight (number of edges)

val astroCommunitiesEdges = edges.
  join(astroCommunities, edges("src") === astroCommunities("id")).
  select(col("dst"), col("label").as("src_label"))

val astroCommunitiesEdges2 = astroCommunitiesEdges.
  join(astroCommunities, astroCommunitiesEdges("dst") === astroCommunities("id")).
  select(col("src_label"), col("label").as("dst_label"))

astroCommunitiesEdges2.
  map(row => (row, 1L)).
  reduce(_ + _).show

val astroCommunitiesEdges3 = astroCommunitiesEdges2.
  filter("src_label <> dst_label").
  map(row => ((row.getLong(0), row.getLong(1)), 1L)).
  groupByKey(_._1).
  reduceGroups((a, b) => (a._1, a._2 + b._2)).
  map(row => (row._2._1._1, row._2._1._2, row._2._2)).
  select(
    col("_1").as("src"),
    col("_2").as("dst"),
    col("_3").as("weight")
  )

astroCommunitiesEdges3.
  repartition(1).
  write.
  csv("ca-AstroPh_communities_edges.csv")
